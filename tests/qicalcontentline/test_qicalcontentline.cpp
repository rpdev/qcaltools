#include "qcaltools/qicalcontentline.h"

#include <QObject>
#include <QTest>

class QICalContentLineTest : public QObject
{
  Q_OBJECT

private slots:

  void initTestCase() {}
  void init() {}
  void constructor();
  void name();
  void value();
  void isNull();
  void parameters();
  void addParameter();
  void removeParameter();
  void removeAllParameters();
  void copyConstructor();
  void assignmentOperator();
  void toItem();
  void fromItem();
  void cleanup() {}
  void cleanupTestCase() {}
};




void QICalContentLineTest::constructor()
{
    QICalContentLine line;
    Q_UNUSED(line);
}

void QICalContentLineTest::name()
{
    QICalContentLine line;
    QCOMPARE(line.name(), QString());
    line.setName("foo");
    QCOMPARE(line.name(), QString("foo"));
}

void QICalContentLineTest::value()
{
    QICalContentLine line;
    QCOMPARE(line.value(), QString());
    line.setValue("bar");
    QCOMPARE(line.value(), QString("bar"));
}

void QICalContentLineTest::isNull()
{
    QICalContentLine line;
    QVERIFY(line.isNull());
    line.setValue("bar");
    QVERIFY(line.isNull());
    line.setName("foo");
    QVERIFY(!line.isNull());
    line.setValue(QString());
    QVERIFY(!line.isNull());
}

void QICalContentLineTest::parameters()
{
    QICalContentLine line;
    QCOMPARE(line.parameters(), QList<QICalContentLineParameter>());
}

void QICalContentLineTest::addParameter()
{
    QICalContentLine line;

    QVERIFY(!line.addParameter(QString(), {"bar, baz"}));
    QCOMPARE(line.parameters(), QList<QICalContentLineParameter>());

    QVERIFY(line.addParameter("foo", {"bar", "baz"}));
    QCOMPARE(line.parameters().length(), 1);
    QCOMPARE(line.parameters().first().name(), QString("foo"));
    QCOMPARE(line.parameters().first().values(), QStringList({"bar", "baz"}));

    QVERIFY(line.addParameter("foo", {"bar2", "baz2"}));
    QCOMPARE(line.parameters().length(), 2);
    QCOMPARE(line.parameters()[1].name(), QString("foo"));
    QCOMPARE(line.parameters()[1].values(), QStringList({"bar2", "baz2"}));

    QVERIFY(line.addParameter("foo2", {"bar3", "baz3"}));
    QCOMPARE(line.parameters().length(), 3);
    QCOMPARE(line.parameters()[2].name(), QString("foo2"));
    QCOMPARE(line.parameters()[2].values(), QStringList({"bar3", "baz3"}));
}

void QICalContentLineTest::removeParameter()
{
    QICalContentLine line;
    line.addParameter("foo");
    line.addParameter("foo");
    line.addParameter("bar");
    line.addParameter("baz");
    QVERIFY(!line.removeParameter("oof"));
    QVERIFY(line.removeParameter("foo"));
    QCOMPARE(line.parameters().length(), 3);
    QVERIFY(line.removeParameter(line.parameters().first()));
    QCOMPARE(line.parameters().length(), 2);
    QCOMPARE(line.parameters().at(0).name(), QString("bar"));
    QCOMPARE(line.parameters().at(1).name(), QString("baz"));
    QVERIFY(line.removeParameter("bar"));
    QVERIFY(line.removeParameter("baz"));
    QCOMPARE(line.parameters().length(), 0);
}

void QICalContentLineTest::removeAllParameters()
{
    QICalContentLine line;
    line.addParameter("foo");
    line.addParameter("foo");
    line.addParameter("bar");
    line.addParameter("baz");
    QVERIFY(line.removeAllParameters("bar"));
    QCOMPARE(line.parameters().length(), 3);
    QVERIFY(line.removeAllParameters("foo"));
    QCOMPARE(line.parameters().length(), 1);
    QCOMPARE(line.parameters().first().name(), QString("baz"));
}

void QICalContentLineTest::copyConstructor()
{
    QICalContentLine line;
    line.setName("foo");
    line.setValue("bar");
    QICalContentLine copied(line);
    QCOMPARE(copied.name(), line.name());
    QCOMPARE(copied.value(), line.value());
    line.setName("FOO");
    line.setValue("BAR");
    QCOMPARE(copied.name(), line.name());
    QCOMPARE(copied.value(), line.value());
}

void QICalContentLineTest::assignmentOperator()
{
    QICalContentLine line;
    line.setName("foo");
    line.setValue("bar");
    QICalContentLine copied;
    copied = line;
    QCOMPARE(copied.name(), line.name());
    QCOMPARE(copied.value(), line.value());
    line.setName("FOO");
    line.setValue("BAR");
    QCOMPARE(copied.name(), line.name());
    QCOMPARE(copied.value(), line.value());
}

void QICalContentLineTest::toItem()
{
    QICalContentLine line;
    line.setName("foo");
    line.setValue("bar");
    QICalContentItem item = line;
    Q_UNUSED(item);
}

void QICalContentLineTest::fromItem()
{
    QICalContentLine line;
    line.setName("foo");
    line.setValue("bar");
    line.addParameter("baz1");
    line.addParameter("baz2");
    line.addParameter("baz3");
    QICalContentItem item = line;
    QICalContentLine line2 = item.toLine();
    QCOMPARE(line2.name(), line.name());
    QCOMPARE(line2.value(), line.value());
    QCOMPARE(line2.parameters(), line.parameters());
}

QTEST_MAIN(QICalContentLineTest)
#include "test_qicalcontentline.moc"
