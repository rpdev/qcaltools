#include "qcaltools/qicalcontentitem.h"
#include "qcaltools/qicalcontentline.h"
#include "qcaltools/qicalobject.h"

#include <QObject>
#include <QTest>

class QICalContentItemTest : public QObject
{
  Q_OBJECT

private slots:

  void initTestCase() {}
  void init() {}
  void constructor();
  void isLine();
  void isObject();
  void toLine();
  void toObject();
  void parent();
  void cleanup() {}
  void cleanupTestCase() {}
};




void QICalContentItemTest::constructor()
{
    QICalContentItem item;
    QVERIFY(!item.isValid());
}

void QICalContentItemTest::isLine()
{
    QICalContentItem item;
    QVERIFY(!item.isLine());
}

void QICalContentItemTest::isObject()
{
    QICalContentItem item;
    QVERIFY(!item.isObject());
}

void QICalContentItemTest::toLine()
{
    QICalContentItem item;
    QVERIFY(!item.toLine().isValid());
}

void QICalContentItemTest::toObject()
{
    QICalContentItem item;
    QVERIFY(!item.toObject().isValid());
}

void QICalContentItemTest::parent()
{
    QICalContentItem item;
    QVERIFY(!item.parent().isValid());
}

QTEST_MAIN(QICalContentItemTest)
#include "test_qicalcontentitem.moc"
