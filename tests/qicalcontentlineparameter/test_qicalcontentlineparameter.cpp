#include "qcaltools/QICalContentLineParameter"

#include <QObject>
#include <QTest>

class QICalContentLineParameterTest : public QObject
{
  Q_OBJECT

private slots:

  void initTestCase() {}
  void init() {}
  void constructor();
  void copyConstructor();
  void name();
  void values();
  void equals();
  void unquals();
  void cleanup() {}
  void cleanupTestCase() {}
};




void QICalContentLineParameterTest::constructor()
{
    QICalContentLineParameter param;
    QVERIFY(!param.isValid());

    QICalContentLineParameter param2("foo", {"bar", "baz"});
    QVERIFY(param2.isValid());
    QCOMPARE(param2.name(), QString("foo"));
    QCOMPARE(param2.values(), QStringList({"bar", "baz"}));
}

void QICalContentLineParameterTest::copyConstructor()
{
    QICalContentLineParameter param;
    param.setName("foo");
    param.setValues({"bar"});
    QICalContentLineParameter copied(param);
    QCOMPARE(copied.name(), param.name());
    QCOMPARE(copied.values(), param.values());
    copied.setName("FOO");
    copied.setValues({"BAR"});
    QVERIFY(copied.name() != param.name());
    QVERIFY(copied.values() != param.values());
}

void QICalContentLineParameterTest::name()
{
    QICalContentLineParameter param;
    QCOMPARE(param.name(), QString());
    param.setName("foo");
    QVERIFY(param.isValid());
    QCOMPARE(param.name(), QString("foo"));
}

void QICalContentLineParameterTest::values()
{
    QICalContentLineParameter param;
    QCOMPARE(param.values(), QStringList());
    param.setValues({"foo", "bar"});
    QCOMPARE(param.values(), QStringList({"foo", "bar"}));
}

void QICalContentLineParameterTest::equals()
{
    QCOMPARE(QICalContentLineParameter("foo"),
             QICalContentLineParameter("foo"));
    QCOMPARE(QICalContentLineParameter("foo", {"bar", "baz"}),
             QICalContentLineParameter("foo", {"bar", "baz"}));
    QVERIFY(!(QICalContentLineParameter("foo") ==
             QICalContentLineParameter("FOO")));
    QVERIFY(!(QICalContentLineParameter("foo", {"BAR", "baz"}) ==
             QICalContentLineParameter("foo", {"bar", "baz"})));
}

void QICalContentLineParameterTest::unquals()
{
    QVERIFY(!(QICalContentLineParameter("foo") !=
             QICalContentLineParameter("foo")));
    QVERIFY(!(QICalContentLineParameter("foo", {"bar", "baz"}) !=
             QICalContentLineParameter("foo", {"bar", "baz"})));
    QVERIFY(QICalContentLineParameter("foo") !=
             QICalContentLineParameter("FOO"));
    QVERIFY(QICalContentLineParameter("foo", {"BAR", "baz"}) !=
             QICalContentLineParameter("foo", {"bar", "baz"}));
}

QTEST_MAIN(QICalContentLineParameterTest)
#include "test_qicalcontentlineparameter.moc"
