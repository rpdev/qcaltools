# iCalendar Support

The QCalTools library provides support for reading, manipulating and storing data in the iCalendar format.


## Classes Used to Handle iCalendar Data

The following is an overview of the classes used to represent objects in iCalendar format:


![iCalendar Classes UML Diagram](./icalendar_classes.svg "iCalendar Classes UML Diagram")


In a nutshell:

* The entry point is the `QICalDocument` class. This class also provides methods to parse
  a document from an ICalendar formatted input string and convert a document back to ICalendar
  format.
* The document has arbitrary many `QICalCalendar` objects.
* Each calendar in turn can contain arbitrary many `QICalEvent`, `QICalTodo`, `QICalJournal`,
  `QICalFreeBusy` and `QICalTimeZone` objects.
* All of the above classes are `QICalObject` derived, which ensures all objects implement a
  common interface. Part of that interface is the lines API, i.e. each object (besides possible
  child objects) contains arbitrary many `QICalContentLine` objects, i.e. key-value like
  entries that make up the body of the object.
