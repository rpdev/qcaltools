# Developer Documentation

This document and the ones it links to mainly provide background information for developers to help better understand the design of the project.


## iCalendar Handling

One functionality of the library is parsing and interpreting data stored in the [iCalendar](https://en.wikipedia.org/wiki/ICalendar) format. See the [API Overview](icalendar.md) to learn about the classes that are used for this.
