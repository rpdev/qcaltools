CONFIG *= c++11
VERSION = 0.0.0

defineTest(setupTest) {
    CONFIG += c++11
    TARGET = tst_$${1}test
    CONFIG  += console testcase
    CONFIG  -= app_bundle
    TEMPLATE = app
    DEFINES += SRCDIR=\\\"$$PWD/\\\"
    DEFINES += SRCBINDIR=\\\"$$OUT_PWD/\\\"
    QT += testlib xml concurrent
    QT -= gui
    INCLUDEPATH += $$PWD/../../qcaltools/include
    LIBS += -L$$OUT_PWD/../../qcaltools -lqcaltools

    static_lib {
        win32:!win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../../qcaltools/qcaltools.lib
        else:unix|win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../../qcaltools/libqcaltools.a
    } else {
        win32:!win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../../qcaltools/qcaltools.dll
        else:unix|win32-g++: PRE_TARGETDEPS += $$OUT_PWD/../../qcaltools/libqcaltools.so
    }

    export(CONFIG)
    export(TARGET)
    export(TEMPLATE)
    export(DEFINES)
    export(QT)
    export(INCLUDEPATH)
    export(LIBS)
    export(PRE_TARGETDEPS)
}
