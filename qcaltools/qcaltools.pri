QT += core network

QCALTOOLS_PUBLIC_HEADERS = \
    $$PWD/include/qcaltools/QCalTools \
    $$PWD/include/qcaltools/QICalCalendar \
    $$PWD/include/qcaltools/qicalcalendar.h \
    $$PWD/include/qcaltools/QICalContentItem \
    $$PWD/include/qcaltools/qicalcontentitem.h \
    $$PWD/include/qcaltools/QICalContentLine \
    $$PWD/include/qcaltools/qicalcontentline.h \
    $$PWD/include/qcaltools/QICalContentLineParameter \
    $$PWD/include/qcaltools/qicalcontentlineparameter.h \
    $$PWD/include/qcaltools/QICalDocument \
    $$PWD/include/qcaltools/qicaldocument.h \
    $$PWD/include/qcaltools/QICalEvent \
    $$PWD/include/qcaltools/qicalevent.h \
    $$PWD/include/qcaltools/QICalFreeBusy \
    $$PWD/include/qcaltools/qicalfreebusy.h \
    $$PWD/include/qcaltools/QICalJournal \
    $$PWD/include/qcaltools/qicaljournal.h \
    $$PWD/include/qcaltools/QICalObject \
    $$PWD/include/qcaltools/qicalobject.h \
    $$PWD/include/qcaltools/QICalParseError \
    $$PWD/include/qcaltools/qicalparseerror.h \
    $$PWD/include/qcaltools/QICalTimeZone \
    $$PWD/include/qcaltools/qicaltimezone.h \
    $$PWD/include/qcaltools/QICalTodo \
    $$PWD/include/qcaltools/qicaltodo.h \


QCALTOOLS_PRIVATE_HEADERS = \
    $$PWD/src/qicalcontentitemdata.h \
    $$PWD/src/qicalcontentlineparameterdata.h \
    $$PWD/src/qicalparseerrordata.h \


HEADERS *= \
    $$QCALTOOLS_PUBLIC_HEADERS \
    $$QCALTOOLS_PRIVATE_HEADERS \


SOURCES *= \
    $$PWD/src/qicalcalendar.cpp \
    $$PWD/src/qicalcontentitem.cpp \
    $$PWD/src/qicalcontentitemdata.cpp \
    $$PWD/src/qicalcontentline.cpp \
    $$PWD/src/qicalcontentlineparameter.cpp \
    $$PWD/src/qicalcontentlineparameterdata.cpp \
    $$PWD/src/qicaldocument.cpp \
    $$PWD/src/qicalevent.cpp \
    $$PWD/src/qicalfreebusy.cpp \
    $$PWD/src/qicaljournal.cpp \
    $$PWD/src/qicalobject.cpp \
    $$PWD/src/qicalparseerror.cpp \
    $$PWD/src/qicalparseerrordata.cpp \
    $$PWD/src/qicaltimezone.cpp \
    $$PWD/src/qicaltodo.cpp \


INCLUDEPATH *= include
DEPENDPATH *= include src
