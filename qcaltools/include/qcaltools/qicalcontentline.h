#ifndef QICALCONTENTLINE_H
#define QICALCONTENTLINE_H

#include <QObject>

#include <qcaltools/QICalContentItem>
#include <qcaltools/QICalContentLineParameter>


/**
 * @brief A line (aka a property) of a iCalendar object.
 *
 * This class is used to represent a single line (which usually represents
 * a property) within a QICalObject. Usually, properties of an object
 * are stored as a single line of text (possible containing line
 * continuation character sequences). However, list properties can be
 * represented as multiple lines of test which have the same property name.
 *
 * Each content line consists of a name and a value, where both are strings.
 * Additionally, a line can have arbitrary many parameters, which could be
 * used e.g. to store several versions (e.g. for different languages) of
 * a property.
 */
class QICalContentLine : public QICalContentItem
{
    Q_GADGET
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString value READ value WRITE setValue)
    Q_PROPERTY(bool isNull READ isNull)
public:
    QICalContentLine();

    QString name() const;
    void setName(const QString& name);

    QString value() const;
    void setValue(const QString& value);

    bool isNull() const;

    QList<QICalContentLineParameter> parameters() const;
    bool addParameter(const QString &name,
                      const QStringList& values = QStringList());
    bool removeParameter(const QString& name);
    bool removeParameter(const QICalContentLineParameter& parameter);
    bool removeAllParameters(const QString& name);
};

#endif // QICALCONTENTLINE_H
