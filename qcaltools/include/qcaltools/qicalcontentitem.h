#ifndef QICALCONTENTITEM_H
#define QICALCONTENTITEM_H

#include <QObject>
#include <QSharedPointer>

// Forward declarations:
class QICalContentItemData;
class QICalContentLine;
class QICalObject;

/**
 * @brief Base class for any item which can appear in a iCalendar object.
 *
 * This class serves as base for any item which can occur inside a iCalendar
 * object. It provides a common interface for any such item, allowing
 * them to be stored in one flat list inside the object.
 */
class QICalContentItem
{
    Q_GADGET
    Q_PROPERTY(bool isValid READ isValid)
    Q_PROPERTY(bool isObject READ isObject)
    Q_PROPERTY(bool isLine READ isLine)
    Q_PROPERTY(QICalContentItem parent READ parent)
public:

    /**
     * @brief The type of an item.
     */
    enum ItemType {
        Invalid,  //!< The item is invalid.
        Object,   //!< The item is a iCalendar object.
        Property  //!< The item is a property of an iCalendar object.
    };

    Q_ENUM(ItemType)

    explicit QICalContentItem();
    QICalContentItem(const QICalContentItem& other);
    virtual ~QICalContentItem();
    QICalContentItem& operator =(const QICalContentItem& other);

    ItemType itemType() const;

    bool isValid() const;
    bool isObject() const;
    bool isLine() const;
    QICalObject toObject();
    QICalContentLine toLine();
    QICalContentItem parent() const;

protected:
    QSharedPointer<QICalContentItemData> d;
};

Q_DECLARE_METATYPE(QICalContentItem)

#endif // QICALCONTENTITEM_H
