#ifndef QICALCONTENTLINEPARAMETER_H
#define QICALCONTENTLINEPARAMETER_H


#include <QObject>
#include <QSharedDataPointer>
#include <QString>
#include <QStringList>
#include <QMetaType>


// Forward declaration
class QICalContentLineParameterData;


/**
 * @brief A parameter of a content line within an iCalendar object.
 *
 * This class is used to represent a parameter of a content line (i.e. a
 * QICalContentLine object) within a iCalendar object. It is a low level
 * API class and usually does not need to be used on most scenarios.
 */
class QICalContentLineParameter
{
    Q_GADGET
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QStringList values READ values WRITE setValues)

public:

    explicit QICalContentLineParameter();
    explicit QICalContentLineParameter(
            const QString& name,
            const QStringList& values = QStringList());
    QICalContentLineParameter(const QICalContentLineParameter& other);
    virtual ~QICalContentLineParameter();

    QICalContentLineParameter& operator =(
            const QICalContentLineParameter& other);
    bool operator ==(const QICalContentLineParameter& other) const;
    bool operator !=(const QICalContentLineParameter& other) const;


    QString name() const;
    void setName(const QString& name);

    QStringList values() const;
    void setValues(const QStringList& values);

    bool isValid() const;

private:

    QSharedDataPointer<QICalContentLineParameterData> d;

};

Q_DECLARE_METATYPE(QICalContentLineParameter)

#endif // QICALCONTENTLINEPARAMETER_H
