#ifndef QICALPARSEERROR_H
#define QICALPARSEERROR_H


#include <QSharedDataPointer>


// Forward declarations:
class QICalParseErrorData;


class QICalParseError
{
public:
    QICalParseError();

private:
    QSharedDataPointer<QICalParseErrorData> d;
};

#endif // QICALPARSEERROR_H
