#ifndef QICALDOCUMENT_H
#define QICALDOCUMENT_H


/**
 * @brief An iCalendar document.
 *
 * This class represents iCalendar formatted documents. It serves as the entry
 * point for any such document. A document usually contains one or more
 * calendars, which in turn host further objects and attributes.
 *
 * The document can be used for both creating new documents as well as reading
 * existing ones (and modifying them).
 */
class QICalDocument
{
public:
    QICalDocument();
};

#endif // QICALDOCUMENT_H
