#include "qcaltools/QICalContentLineParameter"

#include "qicalcontentlineparameterdata.h"


/**
 * @brief Constructor.
 *
 * Creates a new parameter which initially is invalid. A parameter can become
 * valid by assigning it a name.
 *
 * @sa isValid()
 * @sa name()
 */
QICalContentLineParameter::QICalContentLineParameter() :
    d(new QICalContentLineParameterData)
{
}


/**
 * @brief Constructor.
 *
 * This is an overloaded constructor which applies the @p name and @p values
 * to the created parameter.
 */
QICalContentLineParameter::QICalContentLineParameter(
        const QString& name, const QStringList& values) :
    d(new QICalContentLineParameterData)
{
    d->name = name;
    d->values = values;
}


/**
 * @brief Copy constructor.
 *
 * Creates a new parameter copying the values of the @p other one.
 */
QICalContentLineParameter::QICalContentLineParameter(
        const QICalContentLineParameter& other) :
    d(other.d)
{
}


/**
 * @brief Destructor.
 */
QICalContentLineParameter::~QICalContentLineParameter()
{
}


/**
 * @brief Copy the values from the @p other parameter into this one.
 */
QICalContentLineParameter&QICalContentLineParameter::operator =(
        const QICalContentLineParameter& other)
{
    d = other.d;
    return *this;
}


/**
 * @brief Check if this parameter is equal to the @p other one.
 */
bool QICalContentLineParameter::operator ==(const QICalContentLineParameter& other) const
{
    return d->name == other.d->name &&
            d->values == other.d->values;
}


/**
 * @brief Check if this parameter is not equal to the @p other one.
 */
bool QICalContentLineParameter::operator !=(const QICalContentLineParameter& other) const
{
    return !(*this == other);
}


/**
 * @brief The name of the parameter.
 *
 * @sa isValid()
 * @sa setName()
 */
QString QICalContentLineParameter::name() const
{
    return d->name;
}


/**
 * @brief Set the parameter name.
 *
 * @sa name()
 * @sa isValid()
 */
void QICalContentLineParameter::setName(const QString& name)
{
    d->name = name;
}


/**
 * @brief The values of the parameter.
 *
 * This is the list of values assigned to the parameter. The list might contain
 * arbitrary many values (including none).
 *
 * @sa setValues()
 */
QStringList QICalContentLineParameter::values() const
{
    return d->values;
}


/**
 * @brief Set the values of the parameter.
 *
 * @sa values()
 */
void QICalContentLineParameter::setValues(const QStringList& values)
{
    d->values = values;
}


/**
 * @brief Test if the parameter is valid.
 *
 * This method returns true of the parameter is valid. A valid parameter must
 * have at least a non-empty name.
 */
bool QICalContentLineParameter::isValid() const
{
    return !d->name.isEmpty();
}
