#include "qcaltools/QICalContentLine"

#include "qicalcontentitemdata.h"


/**
 * @brief Constructor.
 *
 * This creates a new QICalContentLine object. The line is initially empty,
 * meaning it has neither a name nor a value set. Such a line is a null
 * line (@sa isNull()). It can become valid by assigning at least a name.
 */
QICalContentLine::QICalContentLine() : QICalContentItem()
{
    d->itemType = Property;
}


/**
 * @brief The name of the property the line represents.
 *
 * @sa setName()
 */
QString QICalContentLine::name() const
{
    return d->name;
}


/**
 * @brief Set the name of the property the line represents.
 *
 * @sa name()
 */
void QICalContentLine::setName(const QString& name)
{
    d->name = name;
}


/**
 * @brief The value of the property the line represents.
 *
 * This returns the value of the property the line represents.
 * A property can be made of multiple lines; however, this returns only
 * the value of this single line.
 *
 * @sa setValue()
 */
QString QICalContentLine::value() const
{
    return d->value;
}


/**
 * @brief Set the value of the property the line represents.
 *
 * @sa value()
 */
void QICalContentLine::setValue(const QString& value)
{
    d->value = value;
}


/**
 * @brief Returns true of the line is a null line.
 *
 * This returns true in case the line is null. A line is a null line
 * if it has an empty name set.
 *
 * @sa setName()
 */
bool QICalContentLine::isNull() const
{
    return d->name.isEmpty();
}


/**
 * @brief The parameters attached to the content line.
 */
QList<QICalContentLineParameter> QICalContentLine::parameters() const
{
    QList<QICalContentLineParameter> result;
    for (auto child : d->children) {
        result << child.value<QICalContentLineParameter>();
    }
    return result;
}


/**
 * @brief Add a new parameter to the line.
 */
bool QICalContentLine::addParameter(const QString& name, const QStringList& values)
{
    bool result = false;
    if (!name.isEmpty()) {
        QVariant child = QVariant::fromValue(
                    QICalContentLineParameter(name, values));
        d->children.append(child);
        result = true;
    }
    return result;
}


/**
 * @brief Remove a parameter from the line.
 *
 * This removes the first parameter where the name of the parameter equals
 * @p name. If such a parameter is found and removed, the method returns true.
 * If no such parameter is found, the method returns false.
 *
 * Note that if there are multiple parameters having the same name, only
 * the first one is removed.
 */
bool QICalContentLine::removeParameter(const QString& name)
{
    bool result = false;
    for (int i = 0; i < d->children.length(); ++i) {
        auto child = d->children.at(i);
        auto param = child.value<QICalContentLineParameter>();
        if (param.name() == name) {
            d->children.removeAt(i);
            result = true;
            break;
        }
    }
    return result;
}


/**
 * @brief Removes the @p parameter from the line.
 *
 * This is an overloaded method which is identical to calling
 *
 * @code
 * line.removeParameter(parameter.name());
 * @endcode
 */
bool QICalContentLine::removeParameter(const QICalContentLineParameter& parameter)
{
    return removeParameter(parameter.name());
}


/**
 * @brief Removes all parameters with having the given @p name.
 *
 * This method removes all parameters which have the given @p name.
 * If at least one such parameter is found, the method returns true.
 * If no parameter was found matching the name, the method returns false.
 */
bool QICalContentLine::removeAllParameters(const QString& name)
{
    bool result = false;
    for (int i = d->children.length() - 1; i >= 0; --i) {
        auto child = d->children.at(i);
        auto param = child.value<QICalContentLineParameter>();
        if (param.name() == name) {
            d->children.removeAt(i);
            result = true;
        }
    }
    return result;
}
