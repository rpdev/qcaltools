#include "qicalcontentitemdata.h"


QICalContentItemData::QICalContentItemData() :
    itemType(QICalContentItem::Invalid),
    name(),
    value(),
    children(),
    parent()
{
}
