#ifndef QICALPARSEERRORDATA_H
#define QICALPARSEERRORDATA_H


#include <QSharedData>


class QICalParseErrorData : public QSharedData
{
public:
    QICalParseErrorData();
};

#endif // QICALPARSEERRORDATA_H
