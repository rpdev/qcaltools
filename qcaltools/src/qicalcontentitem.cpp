#include "qcaltools/QICalContentItem"

#include "qicalcontentitemdata.h"

#include "qcaltools/QICalContentLine"
#include "qcaltools/QICalObject"


/**
 * @brief Constructor.
 *
 * Creates an invalid QICalContentItem.
 */
QICalContentItem::QICalContentItem() :
    d(new QICalContentItemData)
{
}


/**
 * @brief Creates a new QICalContentItem from the @p other item.
 */
QICalContentItem::QICalContentItem(const QICalContentItem& other) :
    d(other.d)
{
}


/**
 * @brief Destructor.
 */
QICalContentItem::~QICalContentItem()
{
}


/**
 * @brief Copy the properties of the @p other item.
 */
QICalContentItem&QICalContentItem::operator =(const QICalContentItem& other)
{
    d = other.d;
    return *this;
}


/**
 * @brief The type of the item.
 *
 * This returns the type of the item. This is used internally. You can use
 * @sa isObject() and @sa isLine() to check if an item is one or the other.
 *
 * @sa isValid()
 */
QICalContentItem::ItemType QICalContentItem::itemType() const
{
    return d->itemType;
}


/**
 * @brief Indicates if the item is valid.
 *
 * This is true if the item either is a QICalContentLine or a QICalObject.
 */
bool QICalContentItem::isValid() const
{
    return d->itemType != Invalid;
}


/**
 * @brief Indicates if the item is an object.
 *
 * This returns true if the item is a QICalObject.
 */
bool QICalContentItem::isObject() const
{
    return d->itemType == Object;
}


/**
 * @brief Indicates if the item is a content line.
 *
 * This returns true if the item is a QICalContentLine.
 */
bool QICalContentItem::isLine() const
{
    return d->itemType == Property;
}


/**
 * @brief Returns the item as an object.
 *
 * This returns a QICalObject representation of the item. The returned
 * object will be invalid in case the item is not an object.
 *
 * @sa isObject()
 */
QICalObject QICalContentItem::toObject()
{
    QICalObject result;
    if (isObject()) {
        result.d = d;
    } else {
        result.d->itemType = Invalid;
    }
    return result;
}


/**
 * @brief Returns the item as a content line.
 *
 * This returns a QICalContentLine representation of the item. The returned
 * object will be invalid in case the item is not a line.
 *
 * @sa isLine()
 */
QICalContentLine QICalContentItem::toLine()
{
    QICalContentLine result;
    if (isLine()) {
        result.d = d;
    } else {
        result.d->itemType = Invalid;
    }
    return result;
}


/**
 * @brief Returns the parent object the item belongs to.
 *
 * This returns the object to which the item belongs. If the item
 * has not been added to an object, the returned object will be
 * invalid.
 */
QICalContentItem QICalContentItem::parent() const
{
    QICalObject result;
    if (!d->parent.isNull()) {
        result.d = d->parent.toStrongRef();
    } else {
        result.d->itemType = Invalid;
    }
    return result;
}

