#ifndef QICALCONTENTITEMDATA_H
#define QICALCONTENTITEMDATA_H


#include <QSharedPointer>
#include <QVariant>

#include "qcaltools/QICalContentItem"

// Forward declarations:
class QICalContentLine;
class QICalObject;


/**
 * @private
 * @brief Data of a content item.
 *
 * This class is a container for data stored in a QCalContentItem.
 */
class QICalContentItemData
{
    friend class QICalContentItem;
    friend class QICalContentLine;
    friend class QICalObject;

private:
    QICalContentItemData();

    QICalContentItem::ItemType itemType;
    QString name;
    QString value;
    QVariantList children;
    QWeakPointer<QICalContentItemData> parent;
};


/**
 * @brief A shared pointer to a QCalContentItemData object.
 */
typedef QSharedPointer<QICalContentItemData> QICalContentItemDataPtr;

#endif // QICALCONTENTITEMDATA_H
