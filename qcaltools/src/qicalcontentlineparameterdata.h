#ifndef QICALCONTENTLINEPARAMETERDATA_H
#define QICALCONTENTLINEPARAMETERDATA_H


#include <QSharedData>
#include <QString>
#include <QStringList>

#include "qcaltools/qicalcontentlineparameter.h"


/**
 * @brief Data container for QICalContentLineParameter objects.
 */
class QICalContentLineParameterData : public QSharedData
{
    friend class QICalContentLineParameter;

public:
    QICalContentLineParameterData();

private:

    QString     name;
    QStringList values;
};

#endif // QICALCONTENTLINEPARAMETERDATA_H
