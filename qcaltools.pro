TEMPLATE = subdirs

SUBDIRS += \
    tests \
    qcaltools

OTHER_FILES += \
    bin/mk-unittest.py \
    COPYING \
    COPYING.EXCEPTIONS \
    docs/icalendar.md \
    docs/README.md \
    .gitlab-ci.yml \
    README.md \

tests.depends += qcaltools
