# QCalTools - Utility Functions to work with CalDAV and iCalendar

QCalTools is a utility library which is targeted to work with CalDAV and the iCalendar format. It is written in C++ on top of Qt 5.


## Build Status

[![build status](https://gitlab.com/rpdev/qcaltools/badges/master/build.svg)](https://gitlab.com/rpdev/qcaltools/commits/master)


## Design Principles

QCalTools has been started to factor out the implementation of a CalDAV client and parsing of iCalendar files from another project and make the implementation available for other projects as well. Having this background, the library follows these principles:

1. **Be lightweight.** The library is written in C++ on top of Qt. It does not pull in further dependencies, making it easy to integrate into applications.
2. **Follow standard approaches.** The library can be build and installed in Operating System specific ways, enabling it to be installed once and then linked to by several other applications.
3. **Easy integration.** Besides allowing to compile the library standalone and having other applications link against it, the project can be used "inline" in other projects (e.g. by including it as a git submodule), which further eases integration into other projects.
4. **Well tested.** Being a building block for other applications, special care is taken in the library to test as much as possible of the code following a *test driven development* approach.


Note that due to the fact that this library is a factored out part of an actual project, it does not necessarily implement the full iCalendar and CalDAV standards. Support for various functions is added on-the-fly when needed. In case you miss a feature, feel free to implement it and create a pull request.



## Building

To build the library stand-alone, you would use the following recipe:

```bash
mkdir build
cd build
qmake CONFIG+=release ../
make
sudo make install
```

This will build the library with default settings and install it system wide (usually into the location where Qt itself is installed).

There are several configuration options that you can pass to `qmake` to change how the library is build (i.e. for an option `some_option`, you would pass `CONFIG+=some_option` to `qmake`):

| Option     | Description |
| ---------- | ----------- |
| static_lib | Build the library as static library. Default is to build as shared library. |


## Developer Documentation

More background information such as class diagrams can be found in the [doc sub-folder](doc/README.md). Besides that, the APIs are documented inline as well.


## License

The project is licensed under the terms of the GNU GPL version 3 (or any later version at your choice). This allows it to be used in both open source applications as well as in commercial ones (given they comply with the restrictions of the LGPL).

**As a special exception**, you are allowed to link (either dynamically or statically) against this library without having to license your application under the terms of the GNU GPL. This is pretty much comparable to the GNU LGPL license, however, it allows you to use this library even in situations, where the LGPL normally is difficult to apply (such as embedded systems or environments where users cannot easily re-link your application as requested by the LGPL, such as App Stores of various mobile operating systems). *Please note that you are still obliged to open source modified versions of this library as stated in the GNU GPL!*
